AFRAME.registerComponent('scene-component', {

        init: function() {
            var sceneEl = this.el;
            var selectCursorEl = document.querySelector('#selectCursor');
            var unselectCursorEl = document.querySelector('#unselectCursor');
            var cameraEl = document.querySelector('#camera');
            if (sceneEl.hasLoaded) {
                run();
            } else {
                sceneEl.addEventListener('loaded', run);
            }

            function run() {
                console.log('all the nodes are loaded.');

                var loading = document.querySelector('#loadingView');
                if (loading) {
                    loading.parentNode.removeChild(loading);
                    sceneEl.setAttribute('visible', 'true');
                }
            }
        }
    });